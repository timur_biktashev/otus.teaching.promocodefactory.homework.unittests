﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly IFixture fixture;
        private readonly Mock<IRepository<Partner>> partnersRepositoryMock;
        private readonly PartnersController partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            fixture = new Fixture().Customize(new AutoMoqCustomization());
            partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            partnersController = fixture
                .Build<PartnersController>()
                .OmitAutoProperties()
                .Create();
        }

        /// <summary>
        /// 1. Выдаем ошибку 404, если партнер не найден
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = fixture.Create<Guid>();
            var partner = default(Partner);
            partnersRepositoryMock
                .Setup(x => 
                        x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// 2. Выдаем ошибку 400, если партнер заблокирован
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotActive_ShouldReturnBadRequest()
        {
            // Arrange
            var partnerId = fixture.Create<Guid>();
            var partner = fixture
                .Build<Partner>()
                .With(x => x.IsActive, false)
                .Without(x => x.PartnerLimits)
                .Create();

            partnersRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes,
        /// если лимит закончился, то количество не обнуляется
        /// </summary>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public async Task SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodes_ShouldBeZeroIfActiveLimit(bool activeLimit)
        {
            // Arrange
            var partnerId = fixture.Create<Guid>();
            var partner = fixture
                .Build<Partner>()
                .With(x => x.IsActive, true)
                .With(x => x.NumberIssuedPromoCodes, 10)
                .Without(x => x.PartnerLimits)
                .Create();
            var limit = fixture
                .Build<PartnerPromoCodeLimit>()
                .With(x => x.PartnerId, partner.Id)
                .With(x => x.Partner, partner)
                .With(x => x.CancelDate,
                    () => activeLimit ? (DateTime?)null : DateTime.Now)
                .Create();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit> { limit };
            partnersRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should()
                .Match(val => activeLimit ? val == 0 : val != 0);
        }

        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PreviousLimit_ShouldBeClosed()
        {
            // Arrange
            var partnerId = fixture.Create<Guid>();
            var partner = fixture
                .Build<Partner>()
                .With(x => x.IsActive, true)
                .With(x => x.NumberIssuedPromoCodes, 10)
                .Without(x => x.PartnerLimits)
                .Create();
            var limit = fixture
                .Build<PartnerPromoCodeLimit>()
                .With(x => x.PartnerId, partner.Id)
                .With(x => x.Partner, partner)
                .Without(x => x.CancelDate)
                .Create();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit> { limit };
            partnersRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            limit.CancelDate.Should().NotBeNull();
        }

        /// <summary>
        /// 5. Лимит должен быть больше нуля
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Limit_ShouldBeGreaterThenZero()
        {
            // Arrange
            var partnerId = fixture.Create<Guid>();
            var partner = fixture
                .Build<Partner>()
                .With(x => x.IsActive, true)
                .Without(x => x.PartnerLimits)
                .Create();
            partnersRepositoryMock
                .Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = fixture
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 0)
                .Create();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 6. Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Limit_ShouldBeSavedToDb()
        {
            // Arrange
            var serviceProvider = InMemoryConfiguration.GetServiceProvider();
            var partnersRepository = serviceProvider.GetService<IRepository<Partner>>();
            var fixture = new Fixture();
            fixture.Inject<IRepository<Partner>>(partnersRepository);
            var partnersController = fixture
                .Build<PartnersController>()
                .OmitAutoProperties()
                .Create();

            var partner = GetPartner();
            var partnerId = partner.Id;
            await partnersRepository.AddAsync(partner);
            var request = fixture
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 10)
                .Create();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            var partnerFromDb = await partnersRepository.GetByIdAsync(partnerId);
            partnerFromDb.Should().NotBeNull()
                .And.Match<Partner>(x => x.PartnerLimits.Any());
        }

        private Partner GetPartner()
        {
            return new Partner
            {
                Id = Guid.NewGuid(),
                Name = "Отомстители",
                IsActive = true,
                NumberIssuedPromoCodes=10
            };
        }
    }
}